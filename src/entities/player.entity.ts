import TransformComponent, { ITransformProps } from 'components/transform';
import BoxColliderComponent from 'components/box-collider';
import ELayers from 'constants/layers';
import { EntityType } from 'game';
import LayerComponent from 'components/layer';
import Mage from 'assets/characters/prepared/mage.gif';
import PlayerControlsComponent from 'components/player-controls';
import RigidBodyComponent from 'components/rigid-body';
import SpriteComponent from 'components/sprite';
import { game } from 'index';

const PlayerAnimation = {
  idle: 'idle',
  walk: 'walk',
  jump: 'jump',
  falling: 'falling',
};
export type EPlayerAnimation = typeof PlayerAnimation;

export default async (
  transform: Partial<ITransformProps>
): Promise<EntityType> => {
  const player = game.ecs.createEntity();
  const image = await game.loader.loadImage(Mage);

  player.addComponent(new LayerComponent({ layer: ELayers.player }));
  player.addComponent(new TransformComponent(transform));
  player.addComponent(new PlayerControlsComponent());
  player.addComponent(new RigidBodyComponent());
  player.addComponent(
    new BoxColliderComponent({
      size: { width: 50, height: 56 },
    })
  );
  player.addComponent(
    new SpriteComponent<EPlayerAnimation>({
      animations: {
        idle: {
          animation: [
            {
              width: 50,
              height: 56,
              x: 2,
              y: 0,
            },
            {
              width: 57,
              height: 55,
              x: 57,
              y: 1,
            },
            {
              width: 57,
              height: 56,
              x: 119,
              y: 1,
            },
            {
              width: 58,
              height: 55,
              x: 181,
              y: 1,
            },
            {
              width: 57,
              height: 55,
              x: 244,
              y: 1,
            },
            {
              width: 58,
              height: 55,
              x: 306,
              y: 1,
            },
            {
              width: 57,
              height: 55,
              x: 369,
              y: 1,
            },
            {
              width: 57,
              height: 55,
              x: 431,
              y: 1,
            },
            {
              width: 57,
              height: 55,
              x: 493,
              y: 1,
            },
            {
              width: 57,
              height: 55,
              x: 555,
              y: 1,
            },
            {
              width: 57,
              height: 55,
              x: 617,
              y: 1,
            },
            {
              width: 57,
              height: 55,
              x: 679,
              y: 1,
            },
            {
              width: 57,
              height: 55,
              x: 741,
              y: 1,
            },
            {
              width: 50,
              height: 56,
              x: 803,
              y: 0,
            },
          ],
          currentFrame: 0,
          coolDown: 200,
          speed: 150,
        },
        walk: {
          animation: [
            {
              width: 47,
              height: 56,
              x: 0,
              y: 58,
            },
            {
              width: 43,
              height: 58,
              x: 52,
              y: 57,
            },
            {
              width: 41,
              height: 57,
              x: 100,
              y: 57,
            },
            {
              width: 42,
              height: 57,
              x: 146,
              y: 57,
            },
            {
              width: 41,
              height: 55,
              x: 193,
              y: 58,
            },
            {
              width: 40,
              height: 54,
              x: 239,
              y: 59,
            },
          ],
          currentFrame: 0,
          coolDown: 0,
          speed: 150,
        },
        jump: {
          animation: [
            {
              width: 61,
              height: 49,
              x: 0,
              y: 171,
            },
            {
              width: 52,
              height: 64,
              x: 66,
              y: 156,
            },
            {
              width: 50,
              height: 65,
              x: 123,
              y: 155,
            },
            {
              width: 55,
              height: 57,
              x: 178,
              y: 163,
            },
            {
              width: 57,
              height: 61,
              x: 238,
              y: 159,
            },
            {
              width: 60,
              height: 60,
              x: 300,
              y: 160,
            },
            {
              width: 64,
              height: 48,
              x: 365,
              y: 172,
            },
          ],
          currentFrame: 0,
          coolDown: 0,
          speed: 100,
        },
        falling: {
          animation: [
            {
              width: 57,
              height: 61,
              x: 238,
              y: 159,
            },
            {
              width: 60,
              height: 60,
              x: 300,
              y: 160,
            },
          ],
          currentFrame: 0,
          coolDown: 0,
          speed: 200,
        },
      },
      currentAnimationName: 'idle',
      sprite: image,
    })
  );

  return player;
};
