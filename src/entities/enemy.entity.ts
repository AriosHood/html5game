import TransformComponent, { ITransformProps } from 'components/transform';
import AppearanceComponent from 'components/appearance';
import BoxColliderComponent from 'components/box-collider';
import ELayers from 'constants/layers';
import { EntityType } from 'game';
import LayerComponent from 'components/layer';
import { game } from 'index';

export default (position: Partial<ITransformProps>): EntityType => {
  const enemy = game.ecs.createEntity();
  enemy.addComponent(new LayerComponent({ layer: ELayers.enemy }));
  enemy.addComponent(new TransformComponent(position));
  enemy.addComponent(new AppearanceComponent({ height: 20, width: 20 }));
  enemy.addComponent(
    new BoxColliderComponent({
      size: { width: 20, height: 20 },
    })
  );

  return enemy;
};
