import BoxColliderComponent from 'components/box-collider';
import BulletComponent from 'components/bullet';
import ELayers from 'constants/layers';
import { EntityType } from 'game';
import { IVector2 } from 'services/vector2.service';
import LayerComponent from 'components/layer';
import Mage from 'assets/characters/prepared/mage.gif';
import SpriteComponent from 'components/sprite';
import TransformComponent from 'components/transform';
import { game } from 'index';

export interface IBulletOptions {
  spawnPosition: IVector2;
  direction: IVector2;
}

const BulletAnimation = {
  idle: 'idle',
  blow: 'blow',
};
export type EBulletAnimation = typeof BulletAnimation;

export default async (
  ownerId: string,
  options: IBulletOptions
): Promise<EntityType> => {
  const width = 18;
  const height = 3;

  const bullet = game.ecs.createEntity();
  const image = await game.loader.loadImage(Mage);

  bullet.addComponent(new LayerComponent({ layer: ELayers.bullet }));
  bullet.addComponent(
    new BulletComponent(ownerId, { direction: options.direction })
  );

  const rotate =
    (Math.atan2(options.direction.y, options.direction.x) * 180) / Math.PI;

  bullet.addComponent(
    new TransformComponent({
      position: options.spawnPosition,
      rotate: (rotate * Math.PI) / 180,
    })
  );
  bullet.addComponent(
    new SpriteComponent<EBulletAnimation>({
      animations: {
        idle: {
          animation: [
            {
              y: 125,
              x: 0,
              width: 30,
              height: 7,
            },
            {
              y: 125,
              x: 35,
              width: 28,
              height: 8,
            },
            {
              y: 126,
              x: 68,
              width: 27,
              height: 7,
            },
            {
              y: 124,
              x: 100,
              width: 24,
              height: 10,
            },
          ],
          currentFrame: 0,
          coolDown: 200,
          speed: 150,
        },
        blow: {
          animation: [
            {
              y: 124,
              x: 100,
              width: 24,
              height: 10,
            },
            {
              y: 118,
              x: 118,
              width: 18,
              height: 22,
            },
            {
              y: 115,
              x: 152,
              width: 22,
              height: 28,
            },
            {
              y: 115,
              x: 179,
              width: 24,
              height: 29,
            },
            {
              y: 119,
              x: 208,
              width: 28,
              height: 28,
            },
            {
              y: 121,
              x: 241,
              width: 27,
              height: 26,
            },
          ],
          currentFrame: 0,
          coolDown: 200,
          speed: 150,
        },
      },
      currentAnimationName: 'idle',
      sprite: image,
    })
  );
  bullet.addComponent(
    new BoxColliderComponent({
      size: { width, height },
    })
  );

  return bullet;
};
