import BoxColliderComponent, { IBoxCollider } from 'components/box-collider';
import TransformComponent, { ITransformProps } from 'components/transform';
import AppearanceComponent from 'components/appearance';
import ELayers from 'constants/layers';
import { EntityType } from 'game';
import LayerComponent from 'components/layer';
import { game } from 'index';

export interface IBoxOption {
  boxCollider: Partial<IBoxCollider>;
  position: Partial<ITransformProps>;
  layer?: ELayers;
}

export default (options: IBoxOption): EntityType => {
  const { boxCollider, position, layer = ELayers.background } = options || {};
  const { width = 10, height = 10 } = boxCollider.size || {};

  const entity = game.ecs.createEntity();
  entity.addComponent(new LayerComponent({ layer }));
  entity.addComponent(new BoxColliderComponent(boxCollider));
  entity.addComponent(new TransformComponent(position));
  entity.addComponent(new AppearanceComponent({ width, height }));

  return entity;
};
