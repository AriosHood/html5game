import Game, { IGame } from 'game';
import BattlegroundScene from 'scenes/battleground';
import { IScene } from './scenes/scene';

export const game: IGame = new Game();

const resizeHandler = (scene: IScene): void => {
  const bodyRect = document.body.getBoundingClientRect();
  const canvas = scene.game.getCanvasDom();
  canvas.height = bodyRect.height;
  canvas.width = bodyRect.width;

  if (scene.update) {
    scene.update();
  }
};

window.addEventListener('load', (): void => {
  const scene = new BattlegroundScene(game);

  resizeHandler(scene);
  document.body.appendChild(game.getCanvasDom());

  window.addEventListener('resize', (): void => resizeHandler(scene));
});
