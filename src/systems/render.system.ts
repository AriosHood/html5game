import { EntityType, IGame } from 'game';
import ELayers from 'constants/layers';
import { System } from 'ecs';

export default class RenderSystem extends System<EntityType> {
  readonly ctx: CanvasRenderingContext2D;
  readonly entities: EntityType[] = [];

  constructor(game: IGame) {
    super(game.ecs);
    this.ctx = game.ctx;
  }

  test(entity: EntityType): boolean {
    return entity.components.appearance !== undefined;
  }

  preUpdate(): void {
    const { canvas } = this.ctx;

    this.ctx.clearRect(0, 0, canvas.width, canvas.height);
  }

  update(entity: EntityType): void {
    const transformComponent = entity.components.transform;
    const appearanceComponent = entity.components.appearance;
    const layerComponent = entity.components.layer;

    if (!transformComponent || !appearanceComponent || !layerComponent) return;

    const { position, rotate } = transformComponent.props;
    const { width, height } = appearanceComponent.props;
    const { layer } = layerComponent.props;

    this.ctx.save();
    this.ctx.translate(position.x, position.y);
    this.ctx.rotate(rotate);
    this.ctx.fillStyle = 'black';

    if (layer === ELayers.enemy) {
      this.ctx.fillStyle = 'red';
    }

    this.ctx.fillRect(0, 0, width, height);
    this.ctx.restore();

    this.ctx.stroke();
  }

  addEntity(entity: EntityType): void {
    if (this.test(entity)) {
      this.entities.push(entity);
    }
  }
}
