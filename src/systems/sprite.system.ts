import SpriteComponent, { ISpriteObject } from 'components/sprite';
import { EComponents } from 'constants/components';
import { IEntity } from 'ecs/entity';
import { IGame } from 'game';
import { System } from 'ecs';
import TransformComponent from 'components/transform';

export interface IAnimationSystemComponents {
  [EComponents.transform]: TransformComponent;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [EComponents.sprite]: SpriteComponent<any>;
}

type IAnimationSystemType = IEntity<EComponents, IAnimationSystemComponents>;

interface IDrawOptions {
  sprite: HTMLImageElement;
  transform: TransformComponent;
  spriteObject: ISpriteObject;
}

export default class SpriteSystem extends System<IAnimationSystemType> {
  readonly ctx: CanvasRenderingContext2D;
  readonly entities: IAnimationSystemType[] = [];

  constructor(game: IGame) {
    super(game.ecs);
    this.ctx = game.ctx;
  }

  addEntity(entity: IAnimationSystemType): void {
    if (this.test(entity)) {
      this.entities.push(entity);
    }
  }

  test(entity: IAnimationSystemType): boolean {
    const components = entity.components;
    const spriteComponent = components.sprite;
    const transformComponent = components.transform;

    return !!(spriteComponent && transformComponent);
  }

  update(entity: IAnimationSystemType): void {
    const components = entity.components;
    const spriteComponent = components.sprite;
    const { elapsedLastFrame, sprite } = spriteComponent.props;

    const spriteOptions: ISpriteObject = spriteComponent.getCurrentFrame();
    const currentAnimation = spriteComponent.getCurrentAnimation();

    if (currentAnimation) {
      const { speed } = currentAnimation;

      if (elapsedLastFrame + speed < Date.now()) {
        spriteComponent.nextFrame();
      }
    }

    this._draw({
      transform: components.transform,
      spriteObject: spriteOptions,
      sprite,
    });
  }

  private _draw({ transform, sprite, spriteObject }: IDrawOptions): void {
    const { position, rotate, scale } = transform.props;
    const { x, y, height, width } = spriteObject;

    this.ctx.save();
    this.ctx.translate(position.x, position.y);
    this.ctx.rotate(rotate);
    this.ctx.scale(scale.x, scale.y);

    const dx = scale.x === -1 ? width * -1 : 0;
    const dy = scale.y === -1 ? height * -1 : 0;

    this.ctx.drawImage(sprite, x, y, width, height, dx, dy, width, height);

    this.ctx.restore();
  }
}
