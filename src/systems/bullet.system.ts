import Bullet, { EBulletAnimation } from 'entities/bullet.entity';
import CollisionService, { IIntersect } from 'services/collision.service';
import { EntityType, IGame } from 'game';
import BoxColliderComponent from 'components/box-collider';
import BulletComponent from 'components/bullet';
import { EComponents } from 'constants/components';
import ELayers from 'constants/layers';
import Enemy from 'entities/enemy.entity';
import { IEntity } from 'ecs/entity';
import LayerComponent from 'components/layer';
import MouseService from 'services/mouse.service';
import SpriteComponent from 'components/sprite';
import { System } from 'ecs';
import TransformComponent from 'components/transform';
import Vector2 from 'services/vector2.service';

export interface IBulletSystemComponents {
  [EComponents.layer]: LayerComponent;
  [EComponents.bullet]: BulletComponent;
  [EComponents.transform]: TransformComponent;
  [EComponents.boxCollider]: BoxColliderComponent;
  [EComponents.sprite]: SpriteComponent<EBulletAnimation>;
}

type IBulletSystemType = IEntity<EComponents, IBulletSystemComponents>;

export default class BulletSystem extends System<IBulletSystemType> {
  entities: IBulletSystemType[] = [];
  private _lastFire: number = 0;
  private readonly _fireCoolDown = 100;
  private readonly _bulletTimeLife = 10000;
  private readonly _mouse: MouseService = new MouseService();
  private readonly _game: IGame;

  constructor(game: IGame) {
    super(game.ecs);

    this._game = game;
  }

  addEntity(entity: IBulletSystemType): void {
    if (this.test(entity)) {
      this.entities.push(entity);
    }
  }

  test(entity: IBulletSystemType): boolean {
    const components = entity.components;
    const layerComponent = components.layer;
    const bulletComponent = components.bullet;
    const transformComponent = components.transform;
    const boxColliderComponent = components.boxCollider;

    return !!(
      ((layerComponent &&
        layerComponent.props.layer === ELayers.bullet &&
        bulletComponent) ||
        layerComponent.props.layer === ELayers.player) &&
      transformComponent &&
      boxColliderComponent
    );
  }

  update(entity: IBulletSystemType): void {
    const layer = entity.components.layer.props.layer;

    switch (layer) {
      case ELayers.player:
        this._handlePlayer(entity);
        break;
      case ELayers.bullet:
        this._handleBullet(entity);
        break;
    }
  }

  private _handleBullet(entity: IBulletSystemType): void {
    const components = entity.components;
    const { direction, velocity, spawnTime } = components.bullet.props;

    if (Date.now() - spawnTime >= this._bulletTimeLife) {
      entity.remove();
      return;
    }

    const transformComponent = components.transform;
    const spriteComponent = components.sprite;
    const colliderComponent = components.boxCollider.props;
    const { position } = components.transform.props;
    const newPosition = position.addition(
      new Vector2(direction.x * velocity, direction.y * velocity)
    );

    transformComponent.setProperties({
      position: newPosition,
    });

    const currentObject: IIntersect = {
      position: new Vector2(newPosition.x, newPosition.y),
      height: colliderComponent.size.height,
      width: colliderComponent.size.width,
    };

    const enemies = [] as EntityType[];
    this.ecs.entities.forEach(value => {
      const layer = value.components.layer;
      if (layer && layer.props.layer === ELayers.enemy) {
        enemies.push(value);
      }
    });

    // Kill enemy and creating new
    if (enemies.length > 1) {
      return;
    }

    for (const object of enemies) {
      const itemComponents = object.components;
      const itemPosition = itemComponents.transform.props;
      const itemBoxCollider = itemComponents.boxCollider.props;
      const itemTransform = itemComponents.transform.props;

      const item: IIntersect = {
        ...itemPosition,
        height: itemBoxCollider.size.height,
        width: itemBoxCollider.size.width,
      };

      if (CollisionService.intersect(currentObject, item)) {
        if (object.components.layer.props.layer === ELayers.bullet) {
          continue;
        }

        const itemLayer = object.components.layer;
        if (itemLayer && itemLayer.props.layer === ELayers.enemy) {
          const canvas = this._game.canvas;
          let x = Math.random() * canvas.width - 100;
          x = x < 0 ? 0 : x;
          let y = Math.random() * canvas.height + 250;
          y = y > canvas.height - 20 ? canvas.height / 2 : y;

          const { animations, currentAnimationName } = spriteComponent.props;
          spriteComponent.setProperties({
            currentAnimationName: 'blow',
          });

          if (animations && currentAnimationName) {
            transformComponent.setProperties({
              position: itemTransform.position,
            });
          }
          entity.components.bullet.setProperties({ velocity: 0 });

          // TODO this is temporary for demonstration
          setTimeout(() => {
            object.remove();
            entity.remove();
          }, 300);

          this.ecs.addEntity(
            Enemy({ velocity: 3, position: new Vector2(x, y) })
          );
        }
      }
    }
  }

  private async _handlePlayer(entity: IBulletSystemType): Promise<void> {
    const lastShot = this._lastFire + this._fireCoolDown;

    if (!this._mouse.isMouseClicked || lastShot > Date.now()) {
      return;
    }

    const { position } = entity.components.transform.props;
    const mouseCoordinates = this._mouse.getMouseCoordinate();
    const distance = mouseCoordinates.distance(position);
    const deltaVector = mouseCoordinates.subtraction(position);
    const inversionDistance = 1 / distance;
    const direction = new Vector2(
      deltaVector.x * inversionDistance,
      deltaVector.y * inversionDistance
    );

    this.ecs.addEntity(
      await Bullet(entity.id, {
        direction: direction,
        spawnPosition: position,
      })
    );

    this._lastFire = Date.now();
  }
}
