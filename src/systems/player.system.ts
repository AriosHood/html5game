import BoxColliderComponent from 'components/box-collider';
import Character from 'behaviors/character.behavior';
import { EComponents } from 'constants/components';
import ELayers from 'constants/layers';
import { EPlayerAnimation } from 'entities/player.entity';
import { IEntity } from 'ecs/entity';
import { IGame } from 'game';
import KeyboardService from 'services/keyboard.service';
import LayerComponent from 'components/layer';
import Mage from 'behaviors/characters/mage.character';
import PlayerControlsComponent from 'components/player-controls';
import RigidBodyComponent from 'components/rigid-body';
import SpriteComponent from 'components/sprite';
import { System } from 'ecs';
import TransformComponent from 'components/transform';

export interface IPlayerPositionSystemComponents {
  [EComponents.transform]: TransformComponent;
  [EComponents.boxCollider]: BoxColliderComponent;
  [EComponents.layer]: LayerComponent;
  [EComponents.playerControls]: PlayerControlsComponent;
  [EComponents.rigidBody]: RigidBodyComponent;
  [EComponents.sprite]: SpriteComponent<EPlayerAnimation>;
}

type IPlayerSystemType = IEntity<EComponents, IPlayerPositionSystemComponents>;

export default class PlayerSystem extends System<IPlayerSystemType> {
  readonly ctx: CanvasRenderingContext2D;
  readonly entities: IPlayerSystemType[] = [];
  private readonly _characters: Map<string, Character> = new Map();
  private readonly _keyboard: KeyboardService = new KeyboardService();

  constructor(game: IGame) {
    super(game.ecs);
    this.ctx = game.ctx;
  }

  addEntity(entity: IPlayerSystemType): void {
    if (this.test(entity)) {
      this.entities.push(entity);

      if (entity.components.layer.props.layer === ELayers.player) {
        this._characters.set(
          entity.id,
          new Mage(entity, this.entities, this._keyboard)
        );
      }
    }
  }

  test(entity: IPlayerSystemType): boolean {
    const components = entity.components;
    const transformComponent = components.transform;
    const boxColliderComponent = components.boxCollider;
    const layerComponent = components.layer;

    return !!(transformComponent && boxColliderComponent && layerComponent);
  }

  update(entity: IPlayerSystemType): void {
    const character = this._characters.get(entity.id);

    if (!character) {
      return;
    }

    character.update();
  }
}
