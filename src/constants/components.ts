import AppearanceComponent from 'components/appearance';
import BoxColliderComponent from 'components/box-collider';
import BulletComponent from 'components/bullet';
import LayerComponent from 'components/layer';
import PlayerControlsComponent from 'components/player-controls';
import RigidBodyComponent from 'components/rigid-body';
import SpriteComponent from 'components/sprite';
import TransformComponent from 'components/transform';

export enum EComponents {
  transform = 'transform',
  boxCollider = 'boxCollider',
  appearance = 'appearance',
  layer = 'layer',
  playerControls = 'playerControls',
  bullet = 'bullet',
  rigidBody = 'rigidBody',
  sprite = 'sprite',
}

export interface IComponents {
  [EComponents.transform]: TransformComponent;
  [EComponents.boxCollider]: BoxColliderComponent;
  [EComponents.appearance]: AppearanceComponent;
  [EComponents.layer]: LayerComponent;
  [EComponents.playerControls]: PlayerControlsComponent;
  [EComponents.bullet]: BulletComponent;
  [EComponents.rigidBody]: RigidBodyComponent;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [EComponents.sprite]: SpriteComponent<any>;
}
