enum ELayers {
  default,
  background,
  ground,
  wall,
  player,
  enemy,
  bullet,
}

export default ELayers;
