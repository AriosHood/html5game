import Box, { IBoxOption } from 'entities/box.entity';
import { EntityType, IGame } from 'game';
import BulletSystem from 'systems/bullet.system';
import ELayers from 'constants/layers';
import Enemy from 'entities/enemy.entity';
import { ITransformProps } from 'components/transform';
import Player from 'entities/player.entity';
import PlayerSystem from 'systems/player.system';
import RenderSystem from 'systems/render.system';
import Scene from './scene';
import SpriteSystem from 'systems/sprite.system';
import Vector2 from 'services/vector2.service';

export default class BattlegroundScene extends Scene {
  private _walls: EntityType[] = [];

  constructor(game: IGame) {
    super(game);

    this.init();
  }

  async init(): Promise<void> {
    // Adding systems
    this.game.addSystem(new RenderSystem(this.game));
    this.game.addSystem(new PlayerSystem(this.game));
    this.game.addSystem(new BulletSystem(this.game));
    this.game.addSystem(new SpriteSystem(this.game));

    const walls = this._createBarrier();
    walls.forEach(wallOptions => {
      const wall = Box(wallOptions);
      this._walls.push(wall);
      this.game.addEntity(wall);
    });

    // Adding entities
    this.game.addEntity(
      await Player({
        velocity: 2,
        position: new Vector2(100, this.game.canvas.height - 70),
      })
    );
    this.game.addEntity(
      Enemy({ velocity: 3, position: new Vector2(300, 300) })
    );
  }

  update(): void {
    const walls = this._createBarrier();
    this._walls.forEach((wall, index) => {
      if (wall.components.boxCollider) {
        const boxCollider = wall.components.boxCollider;
        wall.components.boxCollider.setProperties(walls[index].boxCollider);

        if (wall.components.appearance) {
          wall.components.appearance.setProperties({
            width: boxCollider.props.size.width,
            height: boxCollider.props.size.height,
          });
        }
      }

      if (wall.components.transform) {
        wall.components.transform.setProperties(walls[index].position);
      }
    });
  }

  private _createBarrier(): IBoxOption[] {
    const { canvas } = this.game;
    const defaultPosition: Partial<ITransformProps> = {
      position: new Vector2(0, 0),
      velocity: 0,
    };
    const verticalBoxColliderSize = {
      boxCollider: {
        size: {
          width: 1,
          height: canvas.height,
        },
      },
    };
    const horizontalBoxColliderSize = {
      boxCollider: {
        size: {
          width: canvas.width,
          height: 1,
        },
      },
    };

    return [
      // top
      {
        ...horizontalBoxColliderSize,
        layer: ELayers.ground,
        position: defaultPosition,
      },
      // right
      {
        ...verticalBoxColliderSize,
        layer: ELayers.wall,
        position: {
          ...defaultPosition,
          position: new Vector2(canvas.width - 15, 0),
        },
      },
      // bottom
      {
        ...horizontalBoxColliderSize,
        layer: ELayers.ground,
        position: {
          ...defaultPosition,
          position: new Vector2(0, canvas.height - 1),
        },
      },
      // left
      {
        ...verticalBoxColliderSize,
        layer: ELayers.wall,
        position: {
          ...defaultPosition,
          position: new Vector2(15, 0),
        },
      },
    ];
  }
}
