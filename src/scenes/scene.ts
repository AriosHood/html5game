import { IGame } from 'game';

export interface IScene {
  game: IGame;
  update?: () => void;
}

export default abstract class Scene implements IScene {
  readonly game: IGame;

  protected constructor(game: IGame) {
    this.game = game;
  }
}
