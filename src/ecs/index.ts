import Entity from './entity';
import { IComponents } from './entity';
import System from './system';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
class ECS<N extends keyof any, C extends IComponents<N>> {
  readonly entities: Map<string, Entity<N, C>> = new Map();
  private _lastUpdate: number = performance.now();
  private _systems: System<Entity<N, C>>[] = [];
  readonly canvas: HTMLCanvasElement;

  constructor(canvas: HTMLCanvasElement) {
    this.canvas = canvas;
  }

  /* ENTITIES METHODS */

  createEntity(): Entity<N, C> {
    return new Entity<N, C>();
  }

  addEntity(entity: Entity<N, C>): void {
    if (this.entities.has(entity.id)) {
      throw Error('Entity with this id already exist!');
    }

    this._systems.forEach(system => {
      system.addEntity(entity);
    });

    this.entities.set(entity.id, entity);
  }

  getEntityById(id: string): Entity<N, C> | null {
    if (this.entities.has(id)) {
      return this.entities.get(id) as Entity<N, C>;
    }

    return null;
  }

  getAllEntities(): Entity<N, C>[] {
    const entities: Entity<N, C>[] = [];
    this.entities.forEach((entity): void => {
      entities.push(entity);
    });

    return entities;
  }

  removeEntity(entity: Entity<N, C>): void {
    this.entities.delete(entity.id);
  }

  removeEntityById(id: string): void {
    this.entities.delete(id);
  }

  /* SYSTEMS METHODS */

  addSystem(system: System<Entity<N, C>>): void {
    this._systems.push(system);

    this.entities.forEach((entity): void => {
      if (system.test(entity)) {
        system.addEntity(entity);
      }
    });
  }

  update(): void {
    const now: number = performance.now();
    const elapsed: number = now - this._lastUpdate;

    for (const system of this._systems) {
      if (system.preUpdate) {
        system.preUpdate();
      }

      system.updateAll(elapsed);

      if (system.postUpdate) {
        system.postUpdate();
      }
    }

    this._lastUpdate = now;
  }
}

export { default as Entity } from './entity';
export { default as System } from './system';
export { default as Component } from './component';
export default ECS;
