import ECS from './index';
import { IEntity } from './entity';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
interface ISystem<ET extends IEntity<any, any>> {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  readonly ecs: ECS<any, any>;
  entities: ET[];
  addEntity(entity: ET): void;
  preUpdate?(): void;
  postUpdate?(): void;
  update(entity: ET, elapsed?: number): void;
  test(entity: ET): boolean;
  updateAll(elapsed: number): void;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export default abstract class System<ET extends IEntity<any, any>>
  implements ISystem<ET> {
  abstract entities: ET[];
  abstract addEntity(entity: ET): void;
  abstract update(entity: ET, elapsed?: number): void;
  abstract test(entity: ET): boolean;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  readonly ecs: ECS<any, any>;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  protected constructor(ecs: ECS<any, any>) {
    this.ecs = ecs;
  }

  preUpdate?(): void;
  postUpdate?(): void;

  updateAll(elapsed: number): void {
    for (const entity of this.entities) {
      if (entity.isRemoved) {
        this.removeEntityById(entity.id);
        continue;
      }

      this.update(entity, elapsed);
    }
  }

  removeEntityById(id: string): void {
    const mainEntityIndex = this.entities.findIndex(entity => entity.id === id);

    if (mainEntityIndex) {
      this.entities.splice(mainEntityIndex, 1);
      this.ecs.removeEntityById(id);
    }
  }
}
