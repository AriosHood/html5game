import Component from './component';
import { v4 } from 'uuid';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type IComponents<N extends keyof any> = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [P in N]?: Component<any, N>;
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type IMappedComponents<N extends keyof any, C extends IComponents<N>> = {
  [P in N]: C[P];
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface IEntity<N extends keyof any, C extends IComponents<N>> {
  id: string;
  isRemoved: boolean;
  components: IMappedComponents<N, C>;
  addComponent(component: C[N]): void;
  removeComponent(componentId: N): void;
  remove(): void;
  print(): void;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export default class Entity<N extends keyof any, C extends IComponents<N>>
  implements IEntity<N, C> {
  readonly id: string;
  isRemoved: boolean = false;
  components: IMappedComponents<N, C> = {} as IMappedComponents<N, C>;

  constructor() {
    this.id = v4();
  }

  addComponent(component: C[N]): void {
    if (component) {
      this.components[component.name] = component;
    }
  }

  removeComponent(componentName: N): void {
    delete this.components[componentName];
  }

  remove(): void {
    this.isRemoved = true;
  }

  print(): void {
    console.log(JSON.stringify(this, null, 4));
  }
}
