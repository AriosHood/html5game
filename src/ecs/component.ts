import merge from 'lodash/merge';
import { v4 } from 'uuid';

export default abstract class Component<P, N> {
  readonly id: string;
  abstract readonly name: N;
  abstract defaultProps(): P;
  private _props: P;

  get props(): P {
    return this._props;
  }

  protected constructor(props?: Partial<P>) {
    this._props = merge<P, Partial<P>>(this.defaultProps(), props || {});
    this.id = v4();
  }

  setProperties(props: Partial<P>): void {
    this._props = merge(this._props, props);
  }
}
