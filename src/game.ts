import ECS, { Entity, System } from 'ecs';
import { EComponents, IComponents } from 'constants/components';
import ResourceLoader, {
  IResourceLoader,
} from 'services/resource-loader.service';
import { IEntity } from 'ecs/entity';

export type EntityType = IEntity<EComponents, IComponents>;
export type ECSType = ECS<EComponents, IComponents>;

export interface IGame {
  ecs: ECSType;
  addEntity: (entity: EntityType) => void;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  addSystem: (system: System<any>) => void;
  getCanvasDom: () => HTMLCanvasElement;
  canvas: HTMLCanvasElement;
  ctx: CanvasRenderingContext2D;
  loader: IResourceLoader;
}

export default class Game implements IGame {
  readonly ecs: ECSType;
  readonly canvas: HTMLCanvasElement;
  readonly ctx: CanvasRenderingContext2D;
  readonly loader: IResourceLoader = new ResourceLoader();

  constructor() {
    this.canvas = document.createElement('canvas');
    this.ctx = this.canvas.getContext('2d') as CanvasRenderingContext2D;
    this.ecs = new ECS<EComponents, IComponents>(this.canvas);

    this.update();
  }

  getCanvasDom(): HTMLCanvasElement {
    return this.canvas;
  }

  update(): void {
    this.ecs.update();

    window.requestAnimationFrame(this.update.bind(this));
  }

  addEntity(entity: Entity<EComponents, IComponents>): void {
    this.ecs.addEntity(entity);
  }

  addSystem(system: System<EntityType>): void {
    this.ecs.addSystem(system);
  }
}
