import Vector2, { IVector2 } from './vector2.service';
import BoxColliderComponent from 'components/box-collider';
import { EComponents } from 'constants/components';
import { IEntity } from 'ecs/entity';
import TransformComponent from 'components/transform';

export interface IIntersect {
  position: IVector2;
  height: number;
  width: number;
}

export interface ICollisionComponents {
  [EComponents.transform]: TransformComponent;
  [EComponents.boxCollider]: BoxColliderComponent;
}

export type ICollisionType = IEntity<EComponents, ICollisionComponents>;

export default class CollisionService {
  static intersect(object1: IIntersect, object2: IIntersect): boolean {
    const { position: pos1, height: h1, width: w1 } = object1;
    const { position: pos2, height: h2, width: w2 } = object2;

    return (
      pos1.x + w1 > pos2.x &&
      pos2.x + w2 > pos1.x &&
      pos1.y + h1 > pos2.y &&
      pos2.y + h2 > pos1.y
    );
  }

  static getPositionWithoutCollisions(
    object: ICollisionType,
    target: ICollisionType,
    savePosition: IVector2
  ): IVector2 {
    const objIntersect = this.fromEntityToIntersect(object);
    const targetIntersect = this.fromEntityToIntersect(target);

    const newPosition = Vector2.fromVector2(objIntersect.position);
    const goodPosition = Vector2.fromVector2(savePosition);

    if (
      this.intersect(
        {
          ...objIntersect,
          position: new Vector2(savePosition.x, newPosition.y),
        },
        targetIntersect
      )
    ) {
      goodPosition.x = newPosition.x;
    }

    if (
      this.intersect(
        {
          ...objIntersect,
          position: new Vector2(newPosition.x, savePosition.y),
        },
        targetIntersect
      )
    ) {
      goodPosition.y = newPosition.y;
    }

    return goodPosition;
  }

  static fromEntityToIntersect(entity: ICollisionType): IIntersect {
    const transformComponent = entity.components.transform;
    const boxColliderComponent = entity.components.boxCollider;

    return {
      position: transformComponent.props.position,
      height: boxColliderComponent.props.size.height,
      width: boxColliderComponent.props.size.width,
    };
  }
}
