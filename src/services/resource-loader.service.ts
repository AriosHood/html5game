export interface IResourceLoader {
  loadImage(path: string): Promise<HTMLImageElement>;
}

export default class ResourceLoader implements IResourceLoader {
  private _cache: { [key: string]: HTMLImageElement } = {};

  loadImage(path: string): Promise<HTMLImageElement> {
    if (this._cache[path]) {
      return Promise.resolve(this._cache[path]);
    }

    return new Promise((resolve, reject) => {
      const image = new Image();

      image.onerror = reject;
      image.onload = (): void => {
        resolve(image);
      };

      image.src = path;
    });
  }

  static loadImage(path: string): Promise<HTMLImageElement> {
    return new Promise((resolve, reject) => {
      const image = new Image();

      image.onerror = reject;
      image.onload = (): void => {
        resolve(image);
      };

      image.src = path;
    });
  }
}
