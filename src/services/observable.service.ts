export type TObserverCallback<T> = (data: T) => void;
export interface IObservable {
  emit: <T>(event: string, data: T) => void;
  on: <T>(event: string, callback: TObserverCallback<T>) => void;
}

class ObservableService implements IObservable {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  observers: Map<string, TObserverCallback<any>[]> = new Map();

  emit<T>(event: string, data: T): void {
    this.observers.forEach(callbacks => {
      callbacks.forEach(callback => {
        callback(data);
      });
    });
  }

  on<T>(event: string, callback: TObserverCallback<T>): void {
    const eventCallbacks = this.observers.get(event) || [];
    this.observers.set(event, [...eventCallbacks, callback]);
  }
}

export default new ObservableService();
