export interface IKeyboardService {
  isKeyPressed(code: string): boolean;
  destruct(): void;
}

interface IKeyboardStore {
  [key: string]: boolean;
}

export default class KeyboardService implements IKeyboardService {
  private readonly _activeKeys: IKeyboardStore = {};

  constructor() {
    window.addEventListener('keypress', this._keyPressHandler);
    window.addEventListener('keyup', this._keyUpHandler);
  }

  destruct(): void {
    window.removeEventListener('keypress', this._keyPressHandler);
    window.removeEventListener('keyup', this._keyUpHandler);
  }

  isKeyPressed(code: string): boolean {
    return this._activeKeys[code];
  }

  private _keyPressHandler = (event: KeyboardEvent): void => {
    const { code } = event;

    this._activeKeys[code] = true;
  };

  private _keyUpHandler = (event: KeyboardEvent): void => {
    const { code } = event;

    this._activeKeys[code] = false;
  };
}
