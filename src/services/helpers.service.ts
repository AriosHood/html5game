export default class Helpers {
  static *addImpulse(force: number, step: number = 0.2): Generator<number> {
    let start = 0;
    let counter = force;
    while (counter >= 0) {
      yield (start += Math.pow(counter, 2));
      counter -= step;
    }
  }
}
