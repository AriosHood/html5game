import Vector2, { IVector2 } from 'services/vector2.service';

export interface IMousedService {
  getMouseCoordinate(): IVector2;
  destruct(): void;
  isMouseClicked: boolean;
}

export default class MouseService implements IMousedService {
  private _x = 0;
  private _y = 0;
  private _isClicked = false;

  get isMouseClicked(): boolean {
    return this._isClicked;
  }

  constructor() {
    window.addEventListener('mousedown', this._handleMouseDown);
    window.addEventListener('mouseup', this._handleMouseUp);
    window.addEventListener('mousemove', this._handleMouseMove);
  }

  destruct(): void {
    window.removeEventListener('mousedown', this._handleMouseDown);
    window.removeEventListener('mouseup', this._handleMouseUp);
    window.removeEventListener('mousemove', this._handleMouseMove);
  }

  getMouseCoordinate(): IVector2 {
    return new Vector2(this._x, this._y);
  }

  private _handleMouseMove = (event: MouseEvent): void => {
    this._x = event.clientX;
    this._y = event.clientY;
  };

  private _handleMouseDown = (): void => {
    this._isClicked = true;
  };

  private _handleMouseUp = (): void => {
    this._isClicked = false;
  };
}
