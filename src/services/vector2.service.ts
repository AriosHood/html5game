export interface IVector2 {
  x: number;
  y: number;
  set: (vector: Partial<IVector2>) => IVector2;
  addition: (vector2: Partial<IVector2>) => IVector2;
  subtraction: (vector2: Partial<IVector2>) => IVector2;
  multiplication: (scalar: number) => IVector2;
  distance: (vector2: IVector2) => number;
}

export default class Vector2 implements IVector2 {
  x: number = 0;
  y: number = 0;

  constructor(x: number = 0, y: number = 0) {
    this.x = x;
    this.y = y;
  }

  static fromVector2(vector: IVector2): Vector2 {
    return new Vector2(vector.x, vector.y);
  }

  set(vector2: Partial<Vector2>): Vector2 {
    if (vector2.x) {
      this.x = vector2.x;
    }

    if (vector2.y) {
      this.y = vector2.y;
    }

    return this;
  }

  addition(vector2: Partial<Vector2>): Vector2 {
    const x = vector2.x ? vector2.x : 0;
    const y = vector2.y ? vector2.y : 0;

    return new Vector2(this.x + x, this.y + y);
  }

  subtraction(vector2: Partial<Vector2>): Vector2 {
    const x = vector2.x ? vector2.x : 0;
    const y = vector2.y ? vector2.y : 0;

    return new Vector2(this.x - x, this.y - y);
  }

  multiplication(scalar: number): Vector2 {
    return new Vector2(this.x * scalar, this.y * scalar);
  }

  distance(vector2: Vector2): number {
    const deltaVector = vector2.subtraction(this);
    return Math.sqrt(Math.pow(deltaVector.x, 2) + Math.pow(deltaVector.y, 2));
  }
}
