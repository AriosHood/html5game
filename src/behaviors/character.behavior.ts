import CollisionService, { IIntersect } from 'services/collision.service';
import BoxColliderComponent from 'components/box-collider';
import { EComponents } from 'constants/components';
import ELayers from 'constants/layers';
import { EPlayerAnimation } from 'entities/player.entity';
import Helpers from 'services/helpers.service';
import { IEntity } from 'ecs/entity';
import KeyboardService from 'services/keyboard.service';
import LayerComponent from 'components/layer';
import PlayerControlsComponent from 'components/player-controls';
import RigidBodyComponent from 'components/rigid-body';
import SpriteComponent from 'components/sprite';
import TransformComponent from 'components/transform';
import Vector2 from 'services/vector2.service';

interface ICharacterComponents {
  [EComponents.transform]: TransformComponent;
  [EComponents.boxCollider]: BoxColliderComponent;
  [EComponents.layer]: LayerComponent;
  [EComponents.playerControls]: PlayerControlsComponent;
  [EComponents.rigidBody]: RigidBodyComponent;
  [EComponents.sprite]: SpriteComponent<EPlayerAnimation>;
}

export type ICharacterType = IEntity<EComponents, ICharacterComponents>;

export interface ICharacter {
  update(): void;
}

export default abstract class Character implements ICharacter {
  private readonly _entities: ICharacterType[];
  private readonly _entity: ICharacterType;
  private readonly _components: ICharacterComponents;
  private readonly _keyboard: KeyboardService;
  private _jumpGenerator: Generator<number> | null = null;
  private _collisionCache: ICharacterType[] = [];
  private _isGrounded: boolean = false;
  private _isInJump: boolean = false;

  protected constructor(
    entity: ICharacterType,
    entities: ICharacterType[],
    keyboardService: KeyboardService
  ) {
    this._entity = entity;
    this._components = entity.components;
    this._entities = entities;
    this._keyboard = keyboardService;
  }

  update(): void {
    // Clearing collision cache
    this._collisionCache = [];

    this._moveHandler();
    this._checkState();
  }

  private _checkState(): void {
    this._isGrounded = false;

    const collisions = this._getCollisions();
    for (let i = 0; i < collisions.length; i++) {
      if (collisions[i].components.layer.props.layer === ELayers.ground) {
        this._isGrounded = true;
        break;
      }
    }

    if (this._isInJump) {
      this._jump();
    }
  }

  private _moveHandler(): void {
    const transformComponent = this._components[EComponents.transform];
    const moveControls = this._components[EComponents.playerControls].props
      .move;
    const { position } = transformComponent.props;
    const oldPosition = new Vector2(position.x, position.y);

    this._gravityHandler();

    transformComponent.setProperties({
      scale: new Vector2(1, 1),
    });

    let isMoved = false;
    Object.values(moveControls).forEach(key => {
      if (this._keyboard.isKeyPressed(key)) {
        this._move(key);
        isMoved = true;
      }
    });

    if (!isMoved && !this._isInJump && this._isGrounded) {
      const spriteComponent = this._components[EComponents.sprite];
      spriteComponent.setAnimation('idle');
    }

    const collisionItem = this._getCollisions().find(
      entity => entity.components.layer.props.layer !== ELayers.bullet
    );

    if (collisionItem) {
      const newPos = CollisionService.getPositionWithoutCollisions(
        this._entity,
        collisionItem,
        oldPosition
      );

      transformComponent.setProperties({
        position: newPos,
      });
    }
  }

  private _getCollisions(): ICharacterType[] {
    if (this._collisionCache.length > 0) {
      return this._collisionCache;
    }

    const entityIntersect = CollisionService.fromEntityToIntersect(
      this._entity
    );

    for (const entity of this._entities) {
      if (this._entity === entity) {
        continue;
      }

      const itemComponents = entity.components;
      const itemPosition = itemComponents.transform.props;
      const itemBoxCollider = itemComponents.boxCollider.props;
      const item: IIntersect = {
        ...itemPosition,
        height: itemBoxCollider.size.height,
        width: itemBoxCollider.size.width,
      };

      if (CollisionService.intersect(entityIntersect, item)) {
        this._collisionCache.push(entity);
      }
    }

    return this._collisionCache;
  }

  private _jump(): void {
    if (!this._isInJump || !this._jumpGenerator) {
      return;
    }

    const transformComponent = this._components[EComponents.transform];
    const { velocity: gravityForce, useGravity } = this._components[
      EComponents.rigidBody
    ].props;
    const { position } = transformComponent.props;
    const gravity = useGravity ? gravityForce : 0;
    const iterator = this._jumpGenerator.next();

    if (!iterator.done) {
      transformComponent.setProperties({
        position: position.set({ y: position.y - iterator.value - gravity }),
      });
    } else {
      this._isInJump = false;
    }
  }

  private *_jumpHandler(): Generator<number> {
    const spriteComponent = this._components.sprite;
    const { velocity: gravityForce } = this._components.rigidBody.props;
    const jumpVelocity = 0.2;
    const jumpGen = Helpers.addImpulse(1.4, jumpVelocity);
    let jump = 0;

    while (jump >= -gravityForce) {
      const rise = jumpGen.next();

      if (!rise.done) {
        spriteComponent.setAnimation('jump');

        yield (jump = rise.value);
      } else {
        if (jump <= 0.5) {
          spriteComponent.setAnimation('falling');
        }

        yield (jump -= jumpVelocity);
      }
    }
  }

  private _move(keyCode: string): void {
    const spriteComponent = this._components[EComponents.sprite];
    const transformComponent = this._components[EComponents.transform];
    const { move: moveControls } = this._components[
      EComponents.playerControls
    ].props;
    const { position, velocity } = transformComponent.props;

    switch (keyCode) {
      case moveControls.up:
        if (this._isGrounded) {
          this._jumpGenerator = this._jumpHandler();
          this._isInJump = true;
        }
        break;
      case moveControls.left:
        transformComponent.setProperties({
          position: position.set({ x: position.x - velocity }),
          scale: new Vector2(-1, 1),
        });

        if (this._isGrounded) {
          spriteComponent.setAnimation('walk');
        }
        break;
      case moveControls.down:
        transformComponent.setProperties({
          position: position.set({ y: position.y + velocity }),
        });
        break;
      case moveControls.right:
        transformComponent.setProperties({
          position: position.set({ x: position.x + velocity }),
        });
        if (this._isGrounded) {
          spriteComponent.setAnimation('walk');
        }
        break;
    }
  }

  private _gravityHandler(): void {
    const spriteComponent = this._components[EComponents.sprite];
    const { velocity: gravityForce, useGravity } = this._components[
      EComponents.rigidBody
    ].props;
    const { position } = this._components[EComponents.transform].props;

    if (useGravity) {
      position.set({ y: position.y + gravityForce });
    }

    if (!this._isGrounded && !this._isInJump) {
      spriteComponent.setAnimation('falling');
    }
  }
}
