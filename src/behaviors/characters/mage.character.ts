import Character, { ICharacterType } from 'behaviors/character.behavior';
import KeyboardService from 'services/keyboard.service';

export default class Mage extends Character {
  constructor(
    entity: ICharacterType,
    entities: ICharacterType[],
    keyboardService: KeyboardService
  ) {
    super(entity, entities, keyboardService);
  }
}
