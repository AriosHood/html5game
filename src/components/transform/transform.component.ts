import Vector2, { IVector2 } from 'services/vector2.service';
import { Component } from 'ecs';
import { EComponents } from 'constants/components';

export interface ITransformProps {
  position: IVector2;
  rotate: number;
  scale: IVector2;
  velocity: number;
}

export interface ITransformComponent {
  setRotate(angel: number): void;
}

export default class TransformComponent
  extends Component<ITransformProps, EComponents>
  implements ITransformComponent {
  defaultProps(): ITransformProps {
    return {
      position: new Vector2(),
      velocity: 5,
      rotate: 0,
      scale: new Vector2(1, 1),
    };
  }
  readonly name = EComponents.transform;

  constructor(props?: Partial<ITransformProps>) {
    super(props);
  }

  setRotate(angel: number): void {
    this.props.rotate = (angel * Math.PI) / 180;
  }
}
