export {
  default,
  ISpriteComponent,
  ISpriteComponentProps,
  IAnimationAnimations,
  ISpriteObject,
} from './sprite.component';
