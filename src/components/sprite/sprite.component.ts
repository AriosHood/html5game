import { Component } from 'ecs';
import { EComponents } from 'constants/components';

export interface ISpriteObject {
  x: number;
  y: number;
  height: number;
  width: number;
}

export interface IAnimationAnimations {
  animation: ISpriteObject[];
  coolDown: number;
  currentFrame: number;
  speed: number;
  sprite?: HTMLImageElement;
}

export type IMappedAnimations<AN extends object> = {
  [key in keyof AN]: IAnimationAnimations;
};

export interface ISpriteComponentProps<AN extends object> {
  animations?: IMappedAnimations<AN>;
  elapsedLastFrame: number;
  currentAnimationName?: keyof AN;
  spriteObject: ISpriteObject;
  sprite: HTMLImageElement;
}

export interface ISpriteComponent<AN extends object> {
  getCurrentAnimation(): IAnimationAnimations | null;
  getCurrentFrame(): ISpriteObject;
  nextFrame(): ISpriteObject;
  setAnimation(name: keyof AN): void;
}

export default class SpriteComponent<AN extends object>
  extends Component<ISpriteComponentProps<AN>, EComponents>
  implements ISpriteComponent<AN> {
  defaultProps(): ISpriteComponentProps<AN> {
    return {
      sprite: new Image(),
      elapsedLastFrame: 0,
      spriteObject: {
        height: 0,
        width: 0,
        x: 0,
        y: 0,
      },
    };
  }
  readonly name = EComponents.sprite;

  constructor(props?: Partial<ISpriteComponentProps<AN>>) {
    super(props);
  }

  getCurrentAnimation(): IAnimationAnimations | null {
    const { animations, currentAnimationName } = this.props;

    if (!animations || !currentAnimationName) {
      return null;
    }

    return animations[currentAnimationName];
  }

  getCurrentFrame(): ISpriteObject {
    const { animations, currentAnimationName, spriteObject } = this.props;

    if (!animations || !currentAnimationName) {
      return spriteObject;
    }

    const currentFrame = animations[currentAnimationName].currentFrame;
    return animations[currentAnimationName].animation[currentFrame];
  }

  nextFrame(): ISpriteObject {
    const currentAnimation = this.getCurrentAnimation();

    if (!currentAnimation) {
      return this.props.spriteObject;
    }

    currentAnimation.currentFrame += 1;

    if (currentAnimation.currentFrame >= currentAnimation.animation.length) {
      currentAnimation.currentFrame = 0;
    }

    this.setProperties({
      elapsedLastFrame: Date.now(),
    });

    return currentAnimation.animation[currentAnimation.currentFrame];
  }

  setAnimation(name: keyof AN): void {
    if (this.props.currentAnimationName === name) {
      return;
    }

    const currentAnimation = this.getCurrentAnimation();

    if (currentAnimation) {
      currentAnimation.currentFrame = 0;
    }

    this.props.currentAnimationName = name;
  }
}
