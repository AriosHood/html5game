import { Component } from 'ecs';
import { EComponents } from 'constants/components';

export interface IRigidBody {
  useGravity: boolean;
  velocity: number;
}

export default class RigidBodyComponent extends Component<
  IRigidBody,
  EComponents
> {
  defaultProps(): IRigidBody {
    return {
      useGravity: true,
      velocity: 5,
    };
  }
  readonly name = EComponents.rigidBody;

  constructor(props?: Partial<IRigidBody>) {
    super(props);
  }
}
