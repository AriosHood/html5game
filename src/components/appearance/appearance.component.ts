import { Component } from 'ecs';
import { EComponents } from 'constants/components';

export interface IAppearance {
  height: number;
  width: number;
}

export default class AppearanceComponent extends Component<
  IAppearance,
  EComponents
> {
  defaultProps(): IAppearance {
    return { height: 0, width: 0 };
  }
  readonly name = EComponents.appearance;

  constructor(props?: Partial<IAppearance>) {
    super(props);
  }
}
