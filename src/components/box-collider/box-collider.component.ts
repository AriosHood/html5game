import { Component } from 'ecs';
import { EComponents } from 'constants/components';

export interface IBoxColliderSize {
  width: number;
  height: number;
}

export interface IBoxColliderCenter {
  x: number;
  y: number;
}

export interface IBoxCollider {
  center: IBoxColliderCenter;
  isTrigger: boolean;
  size: IBoxColliderSize;
}

export default class BoxColliderComponent extends Component<
  IBoxCollider,
  EComponents
> {
  defaultProps(): IBoxCollider {
    return {
      center: { x: 0, y: 0 },
      isTrigger: false,
      size: { width: 0, height: 0 },
    };
  }
  readonly name = EComponents.boxCollider;

  constructor(props?: Partial<IBoxCollider>) {
    super(props);
  }
}
