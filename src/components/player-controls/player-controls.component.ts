import { Component } from 'ecs';
import { EComponents } from 'constants/components';

export interface IPlayerControls {
  move: {
    up: string;
    down: string;
    left: string;
    right: string;
  };
  fire: string;
}

export default class PlayerControlsComponent extends Component<
  IPlayerControls,
  EComponents
> {
  defaultProps(): IPlayerControls {
    return {
      move: {
        down: 'KeyS',
        up: 'KeyW',
        left: 'KeyA',
        right: 'KeyD',
      },
      fire: 'KeyE',
    };
  }
  readonly name = EComponents.playerControls;

  constructor(props?: Partial<IPlayerControls>) {
    super(props);
  }
}
