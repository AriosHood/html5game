import { Component } from 'ecs';
import { EComponents } from 'constants/components';
import ELayers from '../../constants/layers';

export interface ILayer {
  layer: ELayers;
}

export default class LayerComponent extends Component<ILayer, EComponents> {
  defaultProps(): ILayer {
    return { layer: ELayers.default };
  }
  readonly name = EComponents.layer;

  constructor(props?: Partial<ILayer>) {
    super(props);
  }
}
