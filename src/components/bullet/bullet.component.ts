import Vector2, { IVector2 } from 'services/vector2.service';
import { Component } from 'ecs';
import { EComponents } from 'constants/components';

export interface IBullet {
  ownerId: string;
  damage: number;
  velocity: number;
  direction: IVector2;
  readonly spawnTime: number;
}

export default class BulletComponent extends Component<IBullet, EComponents> {
  defaultProps(): IBullet {
    return {
      spawnTime: 0,
      ownerId: '',
      damage: 5,
      direction: new Vector2(0, 0),
      velocity: 5,
    };
  }
  readonly name = EComponents.bullet;

  constructor(ownerId: string, props?: Partial<IBullet>) {
    super({
      ownerId,
      spawnTime: Date.now(),
      ...props,
    });
  }
}
