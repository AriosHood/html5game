module.exports = {
  devServer: {
    compress: true,
    port: 8080,
    hot: true,
  },
};
