const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const wds = require('./dev-server');
const paths = require('./paths');
const modules = require('./modules');

const common = merge([
  {
    entry: './src/index.ts',
    output: {
      path: paths.root('dist'),
      filename: 'index.js',
    },
    resolve: {
      modules:  ['node_modules', paths.appNodeModules].concat(
        modules.additionalModulePaths || []
      ),
      extensions: ['.ts', '.js'],
    },
    module: {
      rules: [
        {
          test: /\.ts$/,
          use: ['awesome-typescript-loader', 'eslint-loader'],
        },
        {
          test: /\.(png|jpe?g|gif)$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                outputPath: 'assets',
              },
            },
          ],
        },
      ],
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: paths.root('src', 'index.html'),
      }),
      new FaviconsWebpackPlugin(paths.root('src', 'static', 'favicon.png')),
    ],
  },
]);

module.exports = (env) => {
  if (env === 'production') {
    return common;
  }

  if (env === 'development') {
    return merge([
      common,
      wds,
    ]);
  }
};
