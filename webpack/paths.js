const path = require('path');
const fs = require('fs');

const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = (relativePath) => path.resolve(appDirectory, relativePath);
const root = path.join.bind(path, appDirectory);

module.exports = {
  appPath: resolveApp('.'),
  appNodeModules: resolveApp('node_modules'),
  appSrc: resolveApp('src'),
  appTsConfig: resolveApp('tsconfig.json'),
  appJsConfig: resolveApp('jsconfig.json'),
  root,
};
